{
    "menus": [
        {
            "title": "图鉴",
            "column": 2,
            "icon": "cuIcon cuIcon-read",
            "key": "handbook",
            "content": [
                {
                    "title": "角色",
                    "key": "user",
                    "png": "/images/wilson.png"
                },
                {
                    "title": "生物",
                    "key": "animal",
                    "png": "/images/pig.png"
                },
                {
                    "title": "制作",
                    "key": "make",
                    "png": "/images/machine.png",
                    "badge": 1
                },
                {
                    "title": "食材",
                    "key": "food",
                    "png": "/images/meat.png"
                },
                {
                    "title": "料理",
                    "key": "cookbook",
                    "png": "/images/stew.png"
                }
            ]
        },
        {
            "title": "地形",
            "column": 1,
            "icon": "cuIcon cuIcon-location",
            "key": "maps",
            "content": [
                {
                    "png": "/images/ground.svg",
                    "title": "地面地形分布",
                    "key": "ground"
                },
                {
                    "png": "/images/ocean.svg",
                    "title": "海洋地形分布",
                    "key": "ocean"
                },
                {
                    "png": "/images/underground.svg",
                    "title": "地下地形分布",
                    "key": "underground"
                }
            ]
        },
        {
            "title": "",
            "column": 1,
            "png": "/images/pot.png",
            "key": "handbook",
            "class": "menus-big",
            "content": [
                {
                    "title": "烹饪锅",
                    "key": "cooking",
                    "png": "/images/pot.png"
                }
            ]
        },
        {
            "title": "攻略",
            "column": 1,
            "permission": 1,
            "icon": "cuIcon cuIcon-form",
            "key": "guide",
            "content": [
                {
                    "png": "/images/survival.png",
                    "title": "生存",
                    "key": "survival"
                },
                {
                    "png": "/images/fight.png",
                    "title": "战斗",
                    "key": "fight"
                },
                {
                    "img": "/images/more.svg",
                    "title": "其他",
                    "key": "more"
                }
            ]
        },
        {
            "title": "探索",
            "column": 2,
            "icon": "cuIcon cuIcon-creative",
            "key": "creative",
            "content": [
                {  
                    "png": "/images/ai.svg", 
                    "class": "scale0_8 margin-auto", 
                    "box": "full", 
                    "title": "AI", 
                    "key": "ai", 
                    "badge":1 
                },
                {
                    "icon": "cuIcon-pic",
                    "box": "full",
                    "title": "壁纸周刊",
                    "key": "wallpaper",
                    "badge": 1
                },
                {
                    "icon": "cuIcon-explore",
                    "box": "full",
                    "title": "网络热图",
                    "key": "network",
                    "permission": true,
                    "badge": 1
                },
                {
                    "icon": "cuIcon-pic",
                    "title": "图片服务",
                    "key": "picture"
                },
                {
                    "img": "/images/wind.svg",
                    "class": "scale0_8",
                    "title": "天气服务",
                    "key": "weather"
                },
                {
                    "pngs": "/images/coupon.svg",
                    "icon": "cuIcon-ticket",
                    "box": "full",
                    "title": "优惠券",
                    "permission": true,
                    "key": "coupon"
                }
            ]
        }
    ],
    "proxy": "https://image.baidu.com/search/down?thumburl=https://baidu.com&url=",
    "updated": 6,
    "expires": {
        "handbook.txt": 3,
        "guide.txt": 2,
        "creative.txt": 9
    },
    "bulletin": [
        {"text": "更新了图鉴 - 制作中 武器、防具、暗影内容。优化了 烹饪锅 食材滚动效果。更多内容正在整理，后续会继续更新。", "css": "padding"},
        {"text": "说明一下:做这个小程序纯属个人爱好,方便自己使用顺便锻炼自己的开发能力。放了一些广告是为了摊平成本,小程序每年需要交审核费以及服务器及其他产生的费用开销。饥荒这款游戏版本众多内容繁杂，有些版本我也没有玩过。找资料和攻略会慢一些。后续也会放一些mod内容和更多联机内容。也欢迎提出修改意见。", "css": "padding"}
    ],
    "fetcher": {
        "creative.txt": "https://raw.gitcode.com/gitcode2eon/zelda/raw/master/kingdom/"
    }
}